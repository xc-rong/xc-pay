package com.xc.pay;

import cn.hutool.core.lang.Assert;
import com.xc.pay.ali.AliPayTemplate;
import com.xc.pay.ali.constant.AliProperties;
import com.xc.pay.common.MessageTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * Filename: AliPayAutoConfiguration.java <br>
 * Description: 支付宝支付相关模板配置类<br>
 *
 * @author: rongrong <br>
 * @version: 1.0 <br>
 */
@Slf4j
@SpringBootConfiguration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@EnableConfigurationProperties(AliPayProperties.class)
public class AliPayAutoConfiguration {

  /**
   * @deprecated <br>
   *     如果spring中没有该bean 则生效 并当开启支付宝支付时才生效
   * @param aliPayProperties 属性配置类
   * @return AliPayTemplate 支付宝模板
   */
  @Bean
  @ConditionalOnMissingBean(AliPayTemplate.class)
  @ConditionalOnProperty(value = "pay.ali.enable", havingValue = "true")
  public AliPayTemplate aliPayTemplate(AliPayProperties aliPayProperties) {
    log.info("开启支付宝支付...");
    final String appid = aliPayProperties.getAppid();
    Assert.notBlank(appid, MessageTemplate.ERROR_TEMPLATE, "appid");
    final String appPrivateKey = aliPayProperties.getAppPrivateKey();
    Assert.notBlank(appPrivateKey, MessageTemplate.ERROR_TEMPLATE, "appPrivateKey");
    final String aliPublicKey = aliPayProperties.getAliPublicKey();
    Assert.notBlank(aliPublicKey, MessageTemplate.ERROR_TEMPLATE, "aliPubliceKey");
    String serverUrl;
    if (aliPayProperties.getSandbox()) {
      log.info(MessageTemplate.PAY_ENVIRONMENT_TEMPLATE, "沙箱环境");
      serverUrl = AliProperties.SANDBOX_URL;
    } else {
      log.info(MessageTemplate.PAY_ENVIRONMENT_TEMPLATE, "正式环境");
      serverUrl = AliProperties.SERVER_URL;
    }
    return AliPayTemplate.build(serverUrl, appid, appPrivateKey, aliPublicKey, aliPayProperties);
  }
}
