package com.xc.pay;

import com.xc.pay.weixin.constant.WxProperties;
import com.xc.pay.weixin.enums.VersionEnum;
import com.xc.pay.weixin.enums.WxPayTypeEnum;
import com.xc.pay.weixin.properties.WxAppPayProperties;
import com.xc.pay.weixin.properties.WxJsApiPayProperties;
import com.xc.pay.weixin.properties.base.WxPayBaseProperties;
import com.xc.pay.weixin.properties.base.WxPayCommonProperties;
import com.xc.pay.weixin.template.WxPayAppTemplate;
import com.xc.pay.weixin.template.WxPayJsApiTemplate;
import com.xc.pay.weixin.util.WxPayTemplateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * Filename: WxPayAutoConfiguration.java <br>
 * Description: 微信支付相关模板配置类<br>
 * 注解： SpringBootConfiguration: 标识 自动装配的类;<br>
 * ConditionalOnWebApplication: 只在有servlet运行环境是生效，内部一共有三个，1.any 表示只要是web环境就起作用
 * 2.SERVLET需要有servlet的运行时环境。3.REACTIVE响应式的web应用。跟mvc已经不是一种思想，代码的方式实现不一样，所以不可以使用。
 * EnableConfigurationProperties: 需要读取配置的配置类
 *
 * @author: rongrong <br>
 * @version: 1.0 <br>
 */
@Slf4j
@SpringBootConfiguration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@EnableConfigurationProperties(WeChatPayProperties.class)
public class WxPayAutoConfiguration {
  /** 模板工具 */
  private final WxPayTemplateUtil wxPayTemplateUtil = new WxPayTemplateUtil();

  /**
   * APP支付模板类
   *
   * @param weChatPayProperties 支付所需属性
   * @return APP模板
   */
  @Bean
  @ConditionalOnMissingBean(WxPayAppTemplate.class)
  @ConditionalOnProperty(value = WxProperties.APP, havingValue = "true")
  public WxPayAppTemplate wxPayAppTemplate(WeChatPayProperties weChatPayProperties) {
    // 获取APP支付的一些信息
    final WxAppPayProperties app = weChatPayProperties.getApp();
    log.info("开启微信{}支付...", WxPayTypeEnum.APP);
    VersionEnum version = app.getVersion();
    setParam(weChatPayProperties, app);
    // 获取所需对象
    return (WxPayAppTemplate) wxPayTemplateUtil.checkObj(app, version, WxPayTypeEnum.APP);
  }

  /**
   * JSAPI支付模板类
   *
   * @param weChatPayProperties 支付所需属性
   * @return JSAPI模板
   */
  @Bean
  @ConditionalOnMissingBean(WxPayJsApiTemplate.class)
  @ConditionalOnProperty(value = WxProperties.JSAPI, havingValue = "true")
  public WxPayJsApiTemplate wxPayJsApiTemplate(WeChatPayProperties weChatPayProperties) {
    // 获取APP支付的一些信息
    final WxJsApiPayProperties jsapi = weChatPayProperties.getJsapi();
    log.info("开启微信{}支付...", WxPayTypeEnum.JSAPI);
    VersionEnum version = jsapi.getVersion();
    setParam(weChatPayProperties, jsapi);
    setParam(weChatPayProperties, jsapi);
    return (WxPayJsApiTemplate) wxPayTemplateUtil.checkObj(jsapi, version, WxPayTypeEnum.JSAPI);
  }

  /**
   * 如果common有值则设置
   *
   * @param weChatPayProperties 所有属性值
   * @param properties 要更改的
   */
  private void setParam(WeChatPayProperties weChatPayProperties, WxPayBaseProperties properties) {
    final WxPayCommonProperties common = weChatPayProperties.getCommon();
    wxPayTemplateUtil.isCommonSetProperties(properties, common);
  }
}
