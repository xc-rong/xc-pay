package com.xc.pay.weixin.properties.base;

import lombok.Data;

/**
 * 公共参数属性配置类
 *
 * properties:
 *  pay.wechat.common.mchid = 商户号
 *  pay.wechat.common.mchKey = 商户key
 * ...
 * yml:
 *  pay:
 *    wechat:
 *      common:
 *        mchid: 商户号
 *        xxx
 * @author rongrong
 * @date 2020/12/1
 */
@Data
public class WxPayCommonProperties {

  /** 商户号 */
  private String mchid;
  /** 商户key */
  private String mchKey;
  /** apiV3秘钥值 */
  private String apiV3Key;
  /** 证书序列号 */
  private String certSerialNumber;
  /** p12证书文件的绝对路径或者以classpath:开头的类路径 */
  private String keyPath;
  /** apiclient_key.pem证书文件的绝对路径或者以classpath:开头的类路径 */
  private String privateKeyPath;
  /** apiclient_cert.pem证书文件的绝对路径或者以classpath:开头的类路径 */
  private String privateCertPath;
  /** 证书自动更新时间 默认60分 */
  private int certAutoUpdateTime = 60;
}
