package com.xc.pay.weixin.properties;

import com.xc.pay.weixin.properties.base.WxPayBaseProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * JSAPI支付所需属性类
 *
 * @author rongrong
 * @date 2020/11/27
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxJsApiPayProperties extends WxPayBaseProperties {}
