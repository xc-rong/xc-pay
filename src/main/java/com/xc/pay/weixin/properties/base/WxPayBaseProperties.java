package com.xc.pay.weixin.properties.base;

import com.xc.pay.weixin.enums.VersionEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 所有属性类父类
 * 所有支付类型可单独配置商户号以及证书apiV3密钥值等信息，也可以设置公共common参数进行配置
 * properties: pay.wechat.common.mchid = 商户号
 *             pay.wechat.common.mchKey = 商户key ...
 * yml: pay:
 *        wechat:
 *          common:
 *            mchid: 商户号
 * ...等等
 *
 * @author rongrong
 * @date 2020/11/27
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxPayBaseProperties extends WxPayCommonProperties {
  /** 是否开启支付 */
  private boolean enable = false;
  /** 微信支付版本 默认使用新版 */
  private VersionEnum version = VersionEnum.V3;
  /** 应用APPPID */
  private String appid;
}
