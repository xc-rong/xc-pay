package com.xc.pay.weixin.enums;

/**
 * 微信支付版本枚举
 *
 * @author rongrong
 * @date 2020/12/1
 */
public enum VersionEnum {
  V2,
  V3
}
