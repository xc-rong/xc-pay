package com.xc.pay.weixin.enums;

import com.xc.pay.weixin.constant.WxProperties;
import lombok.AllArgsConstructor;

/**
 * 微信各请求url枚举类
 *
 * @author rongrong
 * @date 2020/11/27
 */
@AllArgsConstructor
public enum WxPayUrlEnum {

  /* ==================V3==================== */
  /** APP统一下单 */
  V3_APP_APY("APP统一下单", String.format(WxProperties.PAY_SERVER_URL, "app")),

  /* ==================V2==================== */
  /** APP、JSAPI、扫码、H5、小程序 统一下单 */
  UNIFY_PAY("统一下单", "https://api.mch.weixin.qq.com/pay/unifiedorder"),

  /** 付款码支付 */
  PAYMENT_CODE_PAY("付款码支付", "https://api.mch.weixin.qq.com/pay/micropay"),

  /** 统一查询订单 */
  ORDER_QUERY("统一查询订单", "https://api.mch.weixin.qq.com/pay/orderquery"),

  /** 统一关闭订单 */
  ORDER_CLOSE("统一关闭订单", "https://api.mch.weixin.qq.com/pay/closeorder"),

  /** 统一申请退款 */
  ORDER_REFUND("统一申请退款", "https://api.mch.weixin.qq.com/secapi/pay/refund"),

  /** 统一退款查询 */
  REFUND_QUERY("统一退款查询", "https://api.mch.weixin.qq.com/pay/refundquery");

  public String mes;

  public String url;
}
