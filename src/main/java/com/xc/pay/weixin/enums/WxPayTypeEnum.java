package com.xc.pay.weixin.enums;

import lombok.AllArgsConstructor;

/** 微信支付类型枚举类 */
@AllArgsConstructor
public enum WxPayTypeEnum {
  /** APP支付类型 */
  APP,
  /** JSAPI支付类型 */
  JSAPI,
  /** H5支付(微信外调用) */
  H5,
  /** NATIVE支付(顾客扫码) */
  NATIVEPAY,
  /** 付款码支付 */
  PAYMENT_CODE,
  /** 小程序支付 */
  MINI_PROGRAM;
}
