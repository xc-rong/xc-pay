package com.xc.pay.weixin.constant;

/**
 * 微信常量类
 *
 * @author rongrong
 * @date 2020/11/27
 */
@SuppressWarnings("all")
public class WxProperties {

  /** 配置类属性前缀 */
  public static final String PREFIX = "pay.wechat";
  /** 配置类属性后缀 */
  public static final String ENABLE = "enable";

  /** ===============是否开启该支付================== */
  public static final String APP = PREFIX + ".app." + ENABLE;

  public static final String JSAPI = PREFIX + ".jsapi." + ENABLE;

  public static final String H5 = PREFIX + ".h5." + ENABLE;

  public static final String NATIVE_PAY = PREFIX + ".nativePay." + ENABLE;

  public static final String PAYMENT_CODE = PREFIX + ".paymentCode." + ENABLE;

  public static final String MINI_PROGRAM = PREFIX + ".miniProgram." + ENABLE;

  /** ==================其他常量======================= */
  // 服务器地址
  public static final String SREVER_URL = "https://api.mch.weixin.qq.com";
  // 证书地址路径
  public static final String CERT_URL = "/v3/certificates";
  // 下载证书地址
  public static final String DOWNLOAD_CERT_URL = SREVER_URL + CERT_URL;
  // APP下单地址
  public static final String PAY_SERVER_URL = SREVER_URL + "/v3/pay/transactions/%s";

  /* ===================END====================== */

}
