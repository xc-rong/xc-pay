package com.xc.pay.weixin.v2;

import com.alibaba.fastjson.JSON;
import com.xc.pay.common.MessageTemplate;
import com.xc.pay.weixin.modal.v2.WxNativeModal;
import com.xc.pay.weixin.util.SignCreateUtil;
import com.xc.pay.weixin.util.WxToolUtil;
import com.xc.pay.weixin.util.XMLUtil;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.jdom2.JDOMException;

import java.io.IOException;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Filename: WeixinAppletPay.java <br>
 *
 * <p>Description: 微信小程序支付<br>
 *
 * @author: rongrong <br>
 * @version: 1.0 <br>
 */
@Slf4j
public class WeixinNativePay {

  /**
   * @MethodName: getOrderInfo @Description: 微信扫码支付
   *
   * @param modal 支付参数
   * @return String 支付所需参数
   * @author lenovo
   * @date 2019-12-01
   */
  public static String getOrderInfo(WxNativeModal modal) throws IOException, JDOMException {
    // 请求参数
    TreeMap<String, Object> arrays = new TreeMap<>();
    arrays.put("appid", modal.getAppid());
    arrays.put("mch_id", modal.getMch_id());
    arrays.put("nonce_str", WxToolUtil.getNonceStr());
    arrays.put("body", modal.getBody());
    arrays.put("out_trade_no", modal.getOut_trade_no());
    arrays.put("total_fee", (Double.valueOf(modal.getTotal_fee()) * 100 + "").replace(".0", ""));
    arrays.put("spbill_create_ip", WxToolUtil.getIpAddress());
    arrays.put("notify_url", modal.getNotify_url());
    arrays.put("trade_type", "NATIVE");
    arrays.put("attach", modal.getAttach());

    // 签名
    String sign = SignCreateUtil.createSign(modal.getSecurity(), arrays);
    // 拼接请求参数
    arrays.put("sign", sign);
    // 转换请求参数为XML
    String xml = XMLUtil.parseXML(arrays);
    // 请求预付单id
    OkHttpClient client = new OkHttpClient();
    Request requestOkhttp =
        new Request.Builder()
            .url(MessageTemplate.URL)
            .post(RequestBody.create(MediaType.parse("application/xml"), xml))
            .build();
    Call call = client.newCall(requestOkhttp);
    String result = call.execute().body().string();
    // 结果集
    Map<String, String> resultMap = XMLUtil.parseMap(result);
    log.info("微信扫码支付统一下单接口返回结果" + result);
    // 取得预付单id
    if (resultMap.containsKey("result_code") && resultMap.containsKey("return_code")) {
      String return_code = resultMap.get("return_code");
      String result_code = resultMap.get("result_code");
      if ("SUCCESS".equals(result_code) && "SUCCESS".equals(return_code)) {
        // 取得prepare_id
        String code_url = resultMap.get("code_url");
        log.info("code_url:{}", code_url);
        SortedMap<String, String> orderInfoMap = new TreeMap<>();
        orderInfoMap.put("code_url", code_url);
        return JSON.toJSONString(orderInfoMap);
      } else {
        throw new IOException("no result..");
      }
    } else {
      throw new IOException("no result..");
    }
  }
}
