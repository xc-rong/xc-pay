package com.xc.pay.weixin.v2;

import com.xc.pay.weixin.modal.v2.WxWithdrawModal;
import com.xc.pay.weixin.util.WxToolUtil;
import com.xc.pay.weixin.util.XMLUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Filename: WxWithdraw.java <br>
 *
 * <p>Description: 微信提现<br>
 *
 * @author: rongrong <br>
 * @version: 1.0 <br>
 */
@Slf4j
public class WxWithdraw {

  /**
   * @MethodName: wx_tixian @Description: 微信提现
   *
   * @param
   * @return
   * @author rongrong
   * @date 2019-12-09
   */
  public HashMap wx_tixian(WxWithdrawModal modal) {
    HashMap<String, Object> map = new HashMap<>();
    // 随机字符串
    String nonce_str = WxToolUtil.getNonceStr();
    //    if (StringUtils.isEmpty(modal.getMoney())) {
    //      map.put("status", false);
    //      map.put("msg", "金额错误");
    //      return map;
    //    }
    //    Integer totalMoney = Integer.valueOf(modal.getMoney()) * 100;
    //    if (totalMoney < 1) {
    //      map.put("status", false);
    //      map.put("msg", "提现金额小于一元");
    //      return map;
    //    }
    //    if (totalMoney > 5000) {
    //      map.put("status", false);
    //      map.put("msg", "每日提现金额不得大于5000");
    //      return map;
    //    }
    // ip
    String spbill_create_ip = WxToolUtil.getIpAddress();
    // 参数：开始生成签名
    SortedMap<String, Object> parameters = new TreeMap<String, Object>();
    //    parameters.put("mch_appid", modal.getAppid());
    //    parameters.put("mchid", modal.getMchId());
    parameters.put("nonce_str", nonce_str);
    //    parameters.put("partner_trade_no", modal.getTradeNo());
    //    parameters.put("openid", modal.getOpenid());
    parameters.put("check_name", "NO_CHECK");
    parameters.put("spbill_create_ip", spbill_create_ip);
    //    parameters.put("amount", totalMoney);
    //    parameters.put("desc", modal.getDesc());
    // 签名
    //    String sign = SignCreateUtil.createSign(modal.getSecurity(), parameters);
    // 拼接请求参数
    //    parameters.put("sign", sign);
    // 转换请求参数为XML
    String xml = XMLUtil.parseXML(parameters);
    try {
      CloseableHttpResponse response = null;
      //          HttpUtils.Post(
      //              MessageTemplate.WITHDRAW_URL, xml, modal.getMchId(), modal.getCertUrl(),
      // true);
      String transfersXml = EntityUtils.toString(response.getEntity(), "utf-8");
      log.debug("返回的xml为：{}", transfersXml);
      Map<String, String> transferMap = XMLUtil.parseMap(transfersXml);
      if (transferMap.size() > 0) {
        if (transferMap.get("result_code").equals("SUCCESS")
            && transferMap.get("return_code").equals("SUCCESS")) {
          map.put("status", true);
          map.put("msg", "提现成功");
          return map;
        } else {
          map.put("status", false);
          map.put("msg", "提现失败");
          return map;
        }
      } else {
        map.put("status", false);
        map.put("msg", "参数信息异常");
        return map;
      }
    } catch (Exception e) {
      map.put("status", false);
      map.put("msg", "企业付款异常：" + e.getMessage());
      return map;
    }
  }
}
