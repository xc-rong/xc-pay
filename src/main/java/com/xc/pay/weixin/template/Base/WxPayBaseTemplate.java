package com.xc.pay.weixin.template.Base;

import com.xc.pay.weixin.enums.WxPayTypeEnum;
import com.xc.pay.weixin.modal.v3.base.WxBaseModal;

/**
 * 模板方法接口类
 *
 * @author rongrong
 * @date 2020/12/3
 */
public interface WxPayBaseTemplate extends WxPayTemplate {
  /**
   * V3版本统一下单接口
   * @param baseModal 参数Modal
   * @return Object 待定
   */
  <T> T orderPay(WxBaseModal baseModal);
}
