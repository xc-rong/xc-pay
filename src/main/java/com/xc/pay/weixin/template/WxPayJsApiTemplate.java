package com.xc.pay.weixin.template;

import cn.hutool.json.JSONObject;
import com.xc.pay.weixin.modal.v3.base.WxBaseModal;
import com.xc.pay.weixin.template.Base.WxPayDefaultTemplate;

/**
 * JSAPI模板类
 *
 * @author rongrong
 * @date 2020/12/2
 */
public class WxPayJsApiTemplate extends WxPayDefaultTemplate {

  @Override
  public <T> T orderPay(WxBaseModal baseModal) {
    final JSONObject object = super.orderPay(baseModal);
    return (T) object;
  }
}
