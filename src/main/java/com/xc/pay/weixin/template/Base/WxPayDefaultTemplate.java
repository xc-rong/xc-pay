package com.xc.pay.weixin.template.Base;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.xc.pay.weixin.enums.WxPayTypeEnum;
import com.xc.pay.weixin.modal.v3.base.WxBaseModal;

/**
 * 构造一个空的默认实现类 会实现一些通用方法
 *
 * @author rongrong
 * @date 2020/12/3
 */
public class WxPayDefaultTemplate extends WxPayTemplateClient {

  /**
   * V3版本统一下单接口
   *
   * @param baseModal 参数Modal
   * @return Object 待定
   */
  @Override
  public <T> T orderPay(WxBaseModal baseModal) {
    final JSONObject object = JSONUtil.parseObj(baseModal);
    object.putOpt("appid", properties.getAppid()).putOpt("mchid", properties.getMchid());
    return (T) object;
  }
}
