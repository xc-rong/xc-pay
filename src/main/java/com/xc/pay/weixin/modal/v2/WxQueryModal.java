package com.xc.pay.weixin.modal.v2;

/**
 * Filename: WxQueryModal.java <br>
 *
 * <p>Description: 微信查询订单<br>
 *
 * @author: rongrong <br>
 * @version: 1.0 <br>
 */
public class WxQueryModal {

  /** APPID */
  private String appid;

  /** 商户号 */
  private String mchId;

  /** 商户key */
  private String key;

  /** 订单号 */
  private String tradeno;
}
