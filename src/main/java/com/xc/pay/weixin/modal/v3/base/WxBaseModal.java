package com.xc.pay.weixin.modal.v3.base;

import java.io.Serializable;

/**
 * 父类实现 实际传递子类对象
 *
 * @author rongrong
 * @date 2020/12/2
 */
public abstract class WxBaseModal implements Serializable {}
