package com.xc.pay.weixin.modal.v3;

import com.xc.pay.weixin.modal.v3.base.WxBaseModal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * Filename: WxAppModal.java <br>
 *
 * <p>Description: App支付参数实体类<br>
 *
 * @author: rongrong <br>
 * @version: 1.0 <br>
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class WxAppV3Modal extends WxBaseModal {

  /** ----------------必填参数------------------ */
  /** 商品描述 */
  private String description;
  /** 订单号 */
  private String out_trade_no;
  /** 通知URL必须为直接可访问的URL，不允许携带查询串 */
  private String notify_url;
  /** 订单金额信息 */
  private TotalAmount amount;

  /** ----------------非必填参数------------------ */
  /** 交易结束时间 */
  private String time_expire;
  /** 附加数据 */
  private String attach = "";
  /** 场景信息 */
  private SceneInfo scene_info;

  /** 订单金额信息类 */
  @Data
  @Accessors(chain = true)
  public static class TotalAmount {
    /** 订单金额 单位(分) */
    private Integer total;
    /** 货币类型 */
    private String currency = "CNY";
  }

  /** 场景信息 */
  @Data
  @Accessors(chain = true)
  public static class SceneInfo {
    /** 用户终端IP 如存在场景信息参数 则此字段必填 */
    private String payer_client_ip;
    /** 设备号 */
    private String device_id;
    /** 商户门店信息 */
    private StoreInfo store_info;
  }

  /** 门店信息 */
  @Data
  @Accessors(chain = true)
  public static class StoreInfo {
    /** 门店编号 如存在门店信息参数，则此字段必填 */
    private String id;
    /** 门店名称 */
    private String name;
    /** 地区编码 */
    private String area_code;
    /** 详细商户门店地址 */
    private String address;
  }
}
