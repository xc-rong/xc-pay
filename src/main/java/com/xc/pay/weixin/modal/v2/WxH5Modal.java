package com.xc.pay.weixin.modal.v2;

import lombok.Data;
import lombok.ToString;

/**
 * Filename: WxH5Modal.java <br>
 *
 * <p>Description: 微信h5支付参数实体类<br>
 *
 * @author: rongrong <br>
 * @version: 1.0 <br>
 */
@Data
@ToString
public class WxH5Modal {

  /** 应用appid */
  private String appId;
  /** 应用appSecret */
  private String appSecret;
  /** 商户key */
  private String security;
  /** 商户号 */
  private String mchId;
  /** 回调地址 */
  private String notify_url;
  /** 商品描述 */
  private String body;
  /** 订单号 */
  private String out_trade_no;
  /** 订单金额 单位(元) */
  private String total_fee;
  /** 附加数据 */
  private String attach = "";
  /** 场景信息中网站地址 */
  private String wap_url;
  /** 场景信息中网站名称 */
  private String wap_name;
}
