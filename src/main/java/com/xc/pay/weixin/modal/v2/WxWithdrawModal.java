package com.xc.pay.weixin.modal.v2;

/**
 * Filename: WxWithdrawModal.java <br>
 *
 * <p>Description: 微信提现实体类<br>
 *
 * @author: rongrong <br>
 * @version: 1.0 <br>
 */
public class WxWithdrawModal {

  /** APPID */
  private String appid;

  /** 商户号 */
  private String mchId;

  /** 证书路径 */
  private String certUrl;

  /** 商户key */
  private String security;

  /** 微信用户openid */
  private String openid;

  /** 订单号 */
  private String tradeNo;

  /** 提现金额 */
  private String money;

  /** 提现描述 */
  private String desc;
}
