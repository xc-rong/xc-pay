package com.xc.pay.weixin.modal.v3;

import com.xc.pay.weixin.modal.v3.base.WxBaseModal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Filename: WxJsApiModal.java <br>
 *
 * <p>Description: 微信公众号支付实体参数<br>
 *
 * @author: rongrong <br>
 * @version: 1.0 <br>
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public class WxJsApiV3Modal extends WxBaseModal {

  /** 公众号appid */
  private String appid;
  /** 应用appSecret */
  private String secret;
  /** 商户号 */
  private String mch_id;
  /** 商户key */
  private String security;
  /** 公众号支付回调地址 */
  private String notify_url;
  /** 商品描述 */
  private String body;
  /** 订单号 */
  private String out_trade_no;
  /** 订单金额 单位(元) */
  private String total_fee;
  /** 微信用户openid */
  private String openid;
  /** 附加数据 */
  private String attach = "";
}
