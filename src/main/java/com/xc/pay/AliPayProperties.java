package com.xc.pay;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Filename: AliPayProperties.java <br>
 * Description: 属性类<br>
 *
 * @author: rongrong <br>
 * @version: 1.0 <br>
 */
@Data
@Accessors(chain = true)
@ConfigurationProperties(prefix = "pay.ali")
public class AliPayProperties {

  /** 是否开启支付宝支付 */
  private Boolean enable = false;
  /** 是否开启沙箱 */
  private Boolean sandbox = false;
  /** 应用Appid */
  private String appid;
  /** 应用私钥 */
  private String appPrivateKey;
  /** 支付宝公钥 */
  private String aliPublicKey;
  /** 支付宝商户合作伙伴ID 2088开头 */
  private String serverId;
}
