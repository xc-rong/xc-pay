package com.xc.pay.common;

/**
 * Filename: MessageTemplate.java <br>
 * Description: 公共消息类<br>
 *
 * @author: rongrong <br>
 * @version: 1.0 <br>
 */
public class MessageTemplate {

  /** 微信支付统一下单URL */
  public static final String URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
  /** 提现url */
  public static final String WITHDRAW_URL =
      "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";
  /** 支付订单查询 */
  public static final String ORDER_PAY_QUERY = "https://api.mch.weixin.qq.com/pay/orderquery";

  /** 参数消息提示 */
  public static final String ERROR_TEMPLATE = "{}不允许为空";

  public static final String PAY_ENVIRONMENT_TEMPLATE = "启动环境：{}";

  public static final String NOT_FOUND_MSG = "证书文件【%s】不存在";
}
