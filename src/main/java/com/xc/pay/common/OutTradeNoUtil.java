package com.xc.pay.common;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Console;
import cn.hutool.core.util.RandomUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 商户网站唯一订单号生成工具类
 *
 * @author: rongrong
 * @version: 1.0
 * @time: 2020-11-23 22:02
 */
public class OutTradeNoUtil {

  private static final String FORMAT = "yyMMdd";

  /**
   * 生成22位订单号 生成规则采用 前缀+一定长度随机数随机插入13位时间戳中
   *
   * @param payType wx：生成微信订单号 ali：生成支付宝订单号
   * @return 最终生成订单号
   */
  public static String createTradeNo(PayTypeEnum payType) {
    // 13位时间串
    final String timeStr = Convert.toStr(System.currentTimeMillis());
    // 补位订单编号
    String randomNumbers = "";
    // 订单号前缀
    StringBuilder prefix = new StringBuilder("");
    if (payType.isWx()) {
      // 微信订单号
      prefix.append(PayTypeEnum.WX.name());
      // 7位随机数字
      randomNumbers = RandomUtil.randomNumbers(7);
    } else if (payType.isAli()) {
      // 支付宝订单号
      prefix.append(PayTypeEnum.ALI.name());
      // 6位随机数字
      randomNumbers = RandomUtil.randomNumbers(6);
    } else {
      // 其他订单号
      // 9位随机数字
      randomNumbers = RandomUtil.randomNumbers(9);
    }
    final String str = appendNewStr(timeStr, randomNumbers);
    // 订单号
    final String tradeNot = prefix.append(str).toString();
    return tradeNot;
  }

  /**
   * 随机生成数字插入位置
   *
   * @param oldStr 13位原始字符
   * @param appendStr 追加字符
   * @return
   */
  private static String appendNewStr(String oldStr, String appendStr) {
    // 分割原始字符串为集合
    final String[] strings = oldStr.split("");
    // 因数组转为list 内部实现是因为继承了AbstractList  不支持  add、remove等方法 所以需要下边一步操作
    final List<String> list = Arrays.asList(strings);
    final ArrayList<String> arrayList = new ArrayList<>(list);
    // 分割原始字符串为数组
    final String[] newStr = appendStr.split("");
    for (String c : newStr) {
      final int index = RandomUtil.randomInt(13);
      arrayList.add(index, c);
    }
    return String.join("", arrayList);
  }

  public static void main(String[] args) {
    final String wx = createTradeNo(PayTypeEnum.WX);
    final String ali = createTradeNo(PayTypeEnum.ALI);
    final String other = createTradeNo(PayTypeEnum.OTHER);
    Console.log(wx);
    Console.log(ali);
    Console.log(other.length());
  }
}
