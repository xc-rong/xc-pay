package com.xc.pay.common;

import lombok.AllArgsConstructor;

/**
 * 支付类型枚举
 *
 * @author: rongrong
 * @version: 1.0
 * @time: 2020-11-23 22:23
 */
@AllArgsConstructor
public enum PayTypeEnum {
  /** 类型 */
  WX("微信", "wx"),
  ALI("支付宝", "ali"),
  OTHER("其他", "other");

  private String mes;
  private String type;

  public boolean isWx(String type){
    return this.type.equalsIgnoreCase(type);
  }

  public boolean isWx(){
    return this == PayTypeEnum.WX;
  }

  public boolean isAli(String type){
    return this.type.equalsIgnoreCase(type);
  }

  public boolean isAli(){
    return this == PayTypeEnum.ALI;
  }

}
