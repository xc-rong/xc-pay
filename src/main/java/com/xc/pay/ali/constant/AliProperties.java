package com.xc.pay.ali.constant;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;

/**
 * 支付宝常量信息
 *
 * @author rongrong
 * @date 2020/11/23
 */
public class AliProperties {

  /** 固定网关url */
  public static final String SERVER_URL = "https://openapi.alipay.com/gateway.do";
  /** 沙箱网关url */
  public static final String SANDBOX_URL = "https://openapi.alipaydev.com/gateway.do";
  /** JSON格式 */
  public static final String FORMAT = "JSON";
  /** 编码格式 */
  public static final String CHARSET = StandardCharsets.UTF_8.name();
  /** 签名类型 目前支持RSA2和RSA，推荐使用RSA2 */
  public static final String SIGNTYPE = "RSA2";
  /** 订单总金额, 支付最低金额 & 支付最高金额 */
  public static final BigDecimal LOWER = new BigDecimal("0.01"),
      UPPER = new BigDecimal("100000000");
  /** 扫码支付场景 bar_code条码支付 wave_code声波支付 */
  public static final String BAR_CODE = "bar_code", WAVE_CODE = "wave_code";
  /** 授权拼接字符串 */
  public static final String AUTH_CONTENT =
      "apiname=com.alipay.account.auth&app_id={}&app_name=mc&auth_type=AUTHACCOUNT&biz_type=openservice&method=alipay.open.auth.sdk.code.get&pid={}&product_id=APP_FAST_LOGIN&scope=kuaijie&target_id={}&sign_type=RSA2";
}
