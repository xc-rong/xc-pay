package com.xc.pay.ali.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * token枚举
 *
 * @author rongrong
 * @date 2020/11/26
 */
@Getter
@AllArgsConstructor
public enum TokenEnum {
  /** 获取token */
  AUTHORIZATION_CODE("获取token", "authorization_code"),
  /** 刷新token */
  REFRESH_TOKEN("刷新token", "refresh_token");

  private final String mes;
  private final String code;

  /**
   * 是否刷新操作
   *
   * @return
   */
  public boolean isRefresh() {
    return REFRESH_TOKEN == this;
  }
}
