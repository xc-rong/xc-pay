package com.xc.pay.ali.enums;

import com.alipay.api.AlipayRequest;
import com.alipay.api.request.*;
import lombok.AllArgsConstructor;

/**
 * 支付宝请求类枚举
 *
 * @author: rongrong
 * @version: 1.0
 * @time: 2020-11-23 20:52
 */
@AllArgsConstructor
public enum AliMethodEnum {

  /*------------------Method----------------------*/
  APP_PAY_METHOD("APP支付", "alipay.trade.app.pay", new AlipayTradeAppPayRequest()),
  TRADE_PRECREATE("统一收单线下交易预创建(顾客扫码)", "alipay.trade.precreate", new AlipayTradePrecreateRequest()),
  TRADE_PAY("统一收单交易支付接口(商家扫码)", "alipay.trade.pay", new AlipayTradePayRequest()),
  TRADE_WAP_PAY("手机网站支付接口", "alipay.trade.wap.pay", new AlipayTradeWapPayRequest()),
  TRADE_PAGE_PAY("统一收单下单并支付页面接口(PC)", "alipay.trade.page.pay", new AlipayTradePagePayRequest()),
  FUND_TRANS_UNI_TRANSRER(
      "单笔转账", "alipay.fund.trans.uni.transfer", new AlipayFundTransUniTransferRequest()),
  GET_TOKEN("获取OR刷新TOKEN", "alipay.system.oauth.token", new AlipaySystemOauthTokenRequest()),
  GET_USER_INFO("获取用户信息", "alipay.user.info.share", new AlipayUserInfoShareRequest()),
  TRADE_REFUND_METHOD("交易退款接口", "alipay.trade.refund", new AlipayTradeRefundRequest()),
  TRADE_QUERY_METHOD("交易查询接口", "alipay.trade.query", new AlipayTradeQueryRequest()),
  TRADE_CREATE("统一收单交易创建接口", "alipay.trade.create", new AlipayTradeCreateRequest()),
  TRADE_CLOSE("统一收单交易关闭接口", "alipay.trade.close", new AlipayTradeCloseRequest()),
  TRADE_ORDER_SETTLE(
      "统一收单交易结算接口", "alipay.trade.order.settle", new AlipayTradeOrderSettleRequest()),
  TRADE_REFUND_QUERY(
      "交易退款查询接口", "alipay.trade.fastpay.refund.query", new AlipayTradeFastpayRefundQueryRequest());
  /*-------------------------END------------------*/

  public final String mes;
  public final String method;
  public final AlipayRequest alipayRequest;

  public boolean isTrue(AliMethodEnum aliMethodEnum) {
    return this == aliMethodEnum;
  }
}
