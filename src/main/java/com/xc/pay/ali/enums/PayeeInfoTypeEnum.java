package com.xc.pay.ali.enums;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;

/**
 * 收款方类型枚举类
 *
 * @author rongrong
 * @date 2020/11/25
 */
@AllArgsConstructor
public enum PayeeInfoTypeEnum {
  /************收款方类型***************/
  ALIPAY_USER_ID("支付宝的会员ID", "ALIPAY_USER_ID"),
  ALIPAY_LOGON_ID("支付宝登录号，支持邮箱和手机号格式", "ALIPAY_LOGON_ID"),
  BANKCARD_NO("银行卡", "BANKCARD_NO");
  /***********END**************/

  private final String mes;

  private final String val;

  public String getVal() {
    return this.val;
  }

  /**
   * 是否银行卡类型
   *
   * @param val 通过val判断是否该枚举类型
   * @return boolean
   */
  public boolean isBankCard(String val) {
    return StrUtil.equals(this.val, val);
  }
}
