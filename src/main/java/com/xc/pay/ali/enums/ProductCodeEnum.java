package com.xc.pay.ali.enums;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;

/**
 * 转账类型举类
 *
 * @author rongrong
 * @date 2020/11/25
 */
@AllArgsConstructor
public enum ProductCodeEnum {
  /***********业务产品码**************/
  TRANS_ACCOUNT_NO_PWD("转账到支付宝", "TRANS_ACCOUNT_NO_PWD"),
  TRANS_BANKCARD_NO_PWD("转账到银行卡", "TRANS_BANKCARD_NO_PWD");
  /***********END**************/

  private final String mes;

  private final String val;

  public String getVal() {
    return this.val;
  }

  /**
   * 根据val判断是否转账到银行卡
   *
   * @param val 值
   * @return boolean
   */
  public boolean isBankCard(String val) {
    return StrUtil.equalsIgnoreCase(this.val, val);
  }
}
