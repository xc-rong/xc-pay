package com.xc.pay.ali.modal;

import com.alipay.api.domain.Participant;
import lombok.Data;

import java.io.Serializable;

/**
 * 参数验证Model
 *
 * @author rongrong
 * @date 2020/11/25
 */
@Data
public class VerifyPayParamModel implements Serializable {

  public static final long serialVersionUID = 42L;

  /** 订单金额 */
  private String totalAmount;

  /** 订单号 */
  private String outTradeNo;

  /** 标题 */
  private String subject;

  /*==================扫码支付==================*/
  /** 授权码 当调用支付宝alipay.trade.pay接口时(对应merchantNativePay方法)参数必填 */
  private String authCode;

  /**
   * 支付场景 条码支付，取值：bar_code 声波支付，取值：wave_code 当调用支付宝alipay.trade.pay接口时(对应merchantNativePay方法)参数必填
   */
  private String scene;

  /*==================转账接口参数======================*/
  /** 订单号 */
  private String outBizNo;
  /** 转账金额 */
  private String transAmount;
  /** 收款方信息 */
  private Participant payeeInfo;
  /** 业务产品码 */
  private String productCode;

  /*==================订单创建独有参数==================*/
  /** 买家的支付宝唯一用户号（2088开头的16位纯数字） */
  private String buyerId;
  /** 买家支付宝账号，和buyer_id不能同时为空 */
  private String buyerLogonId;

  /*==================END==================*/

}
