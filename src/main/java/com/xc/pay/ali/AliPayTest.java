package com.xc.pay.ali;

import cn.hutool.core.lang.Console;
import com.alipay.api.AlipayApiException;
import com.alipay.api.domain.*;
import com.alipay.api.response.*;
import com.xc.pay.AliPayProperties;
import com.xc.pay.ali.constant.AliProperties;
import com.xc.pay.ali.enums.PayeeInfoTypeEnum;
import com.xc.pay.ali.enums.ProductCodeEnum;
import com.xc.pay.common.OutTradeNoUtil;
import com.xc.pay.common.PayTypeEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试类
 *
 * @author rongrong
 * @date 2020/11/26
 */
public class AliPayTest {

  private static final String NOTIFY_URL = "http://www.oyygke.top/";
  private static final String LOGIN_NAME = "bwpiny6452@sandbox.com";
  private static final String APPID = "2016091500517988";
  private static final String SERVER_ID = "2088102175759801";
  private static final String PRIVATE_KEY =
      "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDikCywRSfiLorsovCmORNjTVtAPvVYSEMM1E8Ri+sruiF3hIzRTNfsXjLOJtmz7/6+YDQ5o8jPmzZcSFxLg9oEH7BZg6LckBAR0tyUNsxz1h9M9Cd9DjsNbMmRE1n1QCSPH0oB62sFooa5s51/Ak6yVm+o8LeH4vtq8k+BNcivfHjli9A7DSBQoyUuD5gMoDLfnXZ0yYfOCgnY1fsB6rJcZ2Om4FZlGW1uK1nuoz/f2KcIRNjWVJlUbJuyt+JcQLrmQcT60Xs1oxm5dcQ4fuOPj39Vjw7I2hH9IPo4SbxF44ymr8y/lI3I8wbpYS3Qin9d6cdSN/ytkZ+sfE9kiZU3AgMBAAECggEBANY5nQfPXDbx60p1hzalIT/0FSRaY4JmUYAAAAOHWYavKJXqL3x4PHKzXzauZYtOkoFQVhFqXKZxDKWVhi+hLLRZglYhsgz76zXvO8KpTgj5nf8VnYERw6SeCLm0oorTa74ox8Go6DkbxGgy/ejqNZ9LO6lycxd5+GDXXXW/dVL3h2klZLhQKBUQK1ZklE2dL+41nnUp0hnPshnpq1jKmmbCgX/8YO4iTm9vSQ3aeYOo2sR6+iEx2GN3Rvl1Q/cRsUNfYyHMP7IRWs5PpCp6naLiMbbZhP/7uuY1ZwuZpC4cAkRGzE+QSbQPdmgmcM/1Fk4XLys94Jp4WguTdwpRWoECgYEA+OC6bHORR1XMjue09a9S2hXTE0GsYLcVeKDDCv+wl9bHuwBZIQDDWJGRYDqsHSTJTyR0J+N/u5nU5WqKs8Vw68p5pDUMeAe7wnb1KFloXHfbmt/syxW+63GdkF1r9IdVq+h6RNSgsXgHl9AVptQI8ZMPNGGCwGkIp4RZjDhVCMsCgYEA6Qv4Q7saaqPAkxMxrMyKO9smzqGppfvp4Eyl6043P4gVBA37NTdgNXDvq5YS/7TFUcD1HgbxLs9ZVoCCEDLp0UTTiUUDEqdBpP4+1Ju9EA8tOwLoBAHkghVk0jh1iyp+FCqjtwUTWfOd22csF+oKSTwoQE6UiKSr6rj32oPAU8UCgYEApZJcc+ourJaYEDp2WTzakdwTIKyAUZd74VSSba7NfTaWigIhSaOIUlQFs09VUc43Xz+RRzhCsjwVNss6m36vMS7y3husv+3lK+qYDs/sbhgPMWnnzvQYn+pmOLTuLTAb/tIbBc+vWGHSK110ChjmGlELvH1izz8RUuDp8i9H4yECgYBEwSJKoT3G7m7XNVd3epTh2eMWHT3W2X6DjTl6NYY5OSWyH4nYhnDc82fSQKF0TVnVp87pP/UR8VyTF7DHpK207tBV9EVW0H+edOAGi7f4r/GRMmdPH1vSQQuQtdV/7FZsDrRkP5wY8rXE0CiDGapVNulNJ6HMuoB57H47OpmYXQKBgEbpEl34sZeQ97zK8RCuC4oUFxFp1QtXeeB9/t0sl9HR5vnGcKrzHWjbg+cODf1Ow0KTENrIql3CCfPk4fWkpEHSyPrsQLRuz4GESRSxd5oEmbtIM5u6IqlF3rdeBHDoly8l0SapA5Gqa2fcrzudsYnevY99bonqkE7HTUwxbjvX";
  private static final String PUBLIC_KEY =
      "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwOgpv/kXkhBt8cvOwmocBcw50OodL25hYfijPEfzHoagwreQr0kkCFICLNYk5ue0jQSwGx/ZKzsHZixjTNxnF/Nz9JfHiYzQZrzf3ximPfAJ6hjrlHZLKa7g+3ZcnajE6tkzQrDjS8RsvN/ODhOLdphFV0IeXTnzeGbGlUcjVOiFFe1jdrS8artUWKmCqXfifg0e5A6gjLOkH0CXwYL3yQeeGldKalKzlftsKW8xX0axrMnd1zIKDqtUhgBRdpr3ruVVxJy5VE6PWXMM61Ltp6Q+2TABRdukGwNXbyh4UsZBCMLr6SxNVmFGaazIU7bjSmRP0WMe63lc9msu6QDrCwIDAQAB";
  private static final AliPayTemplate TEMPLATE;

  static {
    final AliPayProperties properties = new AliPayProperties();
    properties
        .setEnable(true)
        .setSandbox(true)
        .setAppid(APPID)
        .setServerId(SERVER_ID)
        .setAppPrivateKey(PRIVATE_KEY)
        .setAliPublicKey(PUBLIC_KEY);
    TEMPLATE =
        AliPayTemplate.build(AliProperties.SANDBOX_URL, APPID, PRIVATE_KEY, PUBLIC_KEY, properties);
  }

  public static void appPay() {
    final AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
    // 内容
    model.setBody("测试");
    // 订单号
    final String tradeNo = OutTradeNoUtil.createTradeNo(PayTypeEnum.ALI);
    System.err.println("订单号：" + tradeNo);
    model.setOutTradeNo(tradeNo);
    // 标题
    model.setSubject("支付测试");
    // 支付金额
    model.setTotalAmount("99");
    try {
      final String sign = TEMPLATE.appPay(model, NOTIFY_URL);
      Console.log("成功：{}", sign);
    } catch (AlipayApiException e) {
      Console.log(e.getMessage());
    }
  }

  public static void nativePay() {
    final AlipayTradePrecreateModel model = new AlipayTradePrecreateModel();
    // 内容
    model.setBody("测试");
    // 订单号
    model.setOutTradeNo(System.currentTimeMillis() + "");
    // 标题
    model.setSubject("支付测试");
    // 支付金额
    model.setTotalAmount("0.01");
    try {
      final String sign = TEMPLATE.customerNativePay(model, NOTIFY_URL);
      Console.log("成功：{}", sign);
    } catch (AlipayApiException e) {
      Console.log(e.getMessage());
    }
  }

  public static void merchantNativePay() {
    final AlipayTradePayModel model = new AlipayTradePayModel();
    // 内容
    model.setBody("测试");
    // 订单号
    model.setOutTradeNo(System.currentTimeMillis() + "");
    // 标题
    model.setSubject("支付测试");
    // 支付金额
    model.setTotalAmount("0.01");
    // 支付场景
    model.setScene(AliProperties.BAR_CODE);
    // 支付宝支付条形码下的数字
    model.setAuthCode("287917325685791019");
    try {
      final String sign = TEMPLATE.merchantNativePay(model, NOTIFY_URL);
      Console.log("成功：{}", sign);
    } catch (AlipayApiException e) {
      Console.log(e.getMessage());
    }
  }

  public static void wapPay() {
    final AlipayTradeWapPayModel model = new AlipayTradeWapPayModel();
    // 内容
    model.setBody("测试");
    // 订单号
    model.setOutTradeNo(System.currentTimeMillis() + "");
    // 标题
    model.setSubject("支付测试");
    // 支付金额
    model.setTotalAmount("0.01");
    try {
      final String sign = TEMPLATE.wapPay(model, NOTIFY_URL, null);
      Console.log("成功：{}", sign);
    } catch (AlipayApiException e) {
      Console.log(e.getMessage());
    }
  }

  public static void pcPay() {
    final AlipayTradePagePayModel model = new AlipayTradePagePayModel();
    // 内容
    model.setBody("测试");
    // 订单号
    model.setOutTradeNo(System.currentTimeMillis() + "");
    // 标题
    model.setSubject("支付测试");
    // 支付金额
    model.setTotalAmount("0.01");
    model.setQrPayMode("4");
    model.setQrcodeWidth(150L);
    try {
      final String sign = TEMPLATE.pcPay(model, NOTIFY_URL);
      Console.log("成功：{}", sign);
    } catch (AlipayApiException e) {
      Console.log(e.getMessage());
    }
  }

  public static void zhuanzhang() {
    final AlipayFundTransUniTransferModel model = new AlipayFundTransUniTransferModel();
    // 内容
    model.setOrderTitle("测试");
    // 订单号
    model.setOutBizNo(System.currentTimeMillis() + "");
    // 支付金额
    model.setTransAmount("0.01");
    final Participant participant = new Participant();
    participant.setIdentityType(PayeeInfoTypeEnum.ALIPAY_LOGON_ID.getVal());
    participant.setIdentity(LOGIN_NAME);
    participant.setName("沙箱环境");
    model.setPayeeInfo(participant);
    try {
      final String sign =
          TEMPLATE.transferAccounts(model, ProductCodeEnum.TRANS_ACCOUNT_NO_PWD, NOTIFY_URL);
      Console.log("成功：{}", sign);
    } catch (AlipayApiException e) {
      Console.log(e.getMessage());
    }
  }

  public static void toCode() {
    try {
      final String sign = TEMPLATE.userInfoAuth();
      Console.log("成功：{}", sign);
    } catch (AlipayApiException e) {
      Console.log(e.getMessage());
    }
  }

  public static void refund() {
    try {
      final AlipayTradeRefundModel model = new AlipayTradeRefundModel();
      model.setOutTradeNo("ALI3816230637006918731");
      model.setRefundAmount("99");
      final AlipayTradeRefundResponse response = TEMPLATE.refundOrder(model);
      Console.log("退款成功：{}", response.getBuyerUserId());
    } catch (AlipayApiException e) {
      Console.log(e.getMessage());
    }
  }

  public static void queryOrder() {
    try {
      final AlipayTradeQueryModel model = new AlipayTradeQueryModel();
      //      model.setOutTradeNo("ALI3160653788044859767");
      model.setTradeNo("2020112622001489180501044646");
      final AlipayTradeQueryResponse response = TEMPLATE.queryOrderInfo(model);
      // WAIT_BUYER_PAY（交易创建，等待买家付款）、TRADE_CLOSED（未付款交易超时关闭，或支付完成后全额退款）、TRADE_SUCCESS（交易支付成功）、TRADE_FINISHED（交易结束，不可退款）
      Console.log("支付宝交易号：{}", response.getTradeNo());
      Console.log("商户交易号：{}", response.getOutTradeNo());
    } catch (AlipayApiException e) {
      Console.log(e.getMessage());
    }
  }

  public static void tradeCreate() {
    try {
      final AlipayTradeCreateModel model = new AlipayTradeCreateModel();
      // 内容
      model.setBody("测试订单创建");
      // 订单号
      final String tradeNo = OutTradeNoUtil.createTradeNo(PayTypeEnum.ALI);
      System.err.println("订单号：" + tradeNo);
      model.setOutTradeNo(tradeNo);
      // 标题
      model.setSubject("创建订单");
      // 支付金额
      model.setTotalAmount("99");
      model.setBuyerLogonId(LOGIN_NAME);
      final AlipayTradeCreateResponse response = TEMPLATE.tardeCreate(model, NOTIFY_URL);
      Console.log("订单号：{}", response.getOutTradeNo());
    } catch (AlipayApiException e) {
      Console.log(e.getMessage());
    }
  }

  public static void tardeClose() {
    try {
      final AlipayTradeCloseModel model = new AlipayTradeCloseModel();
      model.setOutTradeNo("ALI3160653788044859767");
      final AlipayTradeCloseResponse response = TEMPLATE.tardeClose(model);
      Console.log("订单号：{}", response.getOutTradeNo());
    } catch (AlipayApiException e) {
      Console.log(e.getMessage());
    }
  }

  public static void tradeOrderSettle() {
    try {
      final AlipayTradeOrderSettleModel model = new AlipayTradeOrderSettleModel();
      model.setTradeNo("2020112622001489180501044656");
      model.setOutRequestNo(OutTradeNoUtil.createTradeNo(PayTypeEnum.ALI));
      final List<OpenApiRoyaltyDetailInfoPojo> infoPojos = new ArrayList<>();
      final OpenApiRoyaltyDetailInfoPojo pojo = new OpenApiRoyaltyDetailInfoPojo();
      pojo.setTransIn(LOGIN_NAME);
      pojo.setTransInType(LOGIN_NAME);
      pojo.setAmount("10");
      infoPojos.add(pojo);
      model.setRoyaltyParameters(infoPojos);
      final String response = TEMPLATE.tardeOrderSettle(model);
      Console.log("支付宝交易号：{}", response);
    } catch (AlipayApiException e) {
      Console.log(e.getMessage());
    }
  }

  public static void refundQuery() {
    try {
      final AlipayTradeFastpayRefundQueryModel model = new AlipayTradeFastpayRefundQueryModel();
      model.setTradeNo("2020112622001489180501044646");
      model.setOutRequestNo("ALI3816230637006918731");
      final AlipayTradeFastpayRefundQueryResponse response = TEMPLATE.refundQuery(model);
      Console.log(
          "返回：{},{},{}",
          response.getRefundStatus(),
          response.getTotalAmount(),
          response.getRefundAmount());
    } catch (AlipayApiException e) {
      Console.log(e.getMessage());
    }
  }

  public static void main(String[] args) {
    // app支付
    // appPay();
    // 顾客扫码付
    // nativePay();
    // 商家扫码
    // merchantNativePay();
    // 手机网站支付
    // wapPay();
    // PC支付
    // pcPay();
    // 单笔转账
    // zhuanzhang();
    // 退款接口
    // refund();
    // 订单查询
    //     queryOrder();
    // 订单创建
    // tradeCreate();
    // 订单关闭
    // tardeClose();
    // 订单结算
    // tradeOrderSettle();
    // 退款查询
    refundQuery();
  }
}
