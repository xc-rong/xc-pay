package com.xc.pay;

import com.xc.pay.weixin.properties.*;
import com.xc.pay.weixin.properties.base.WxPayCommonProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Filename: WeChatPayProperties.java <br>
 * Description: 属性类<br>
 *
 * @author: rongrong <br>
 * @version: 1.0 <br>
 */
@Data
@ConfigurationProperties(prefix = "pay.wechat")
public class WeChatPayProperties {

  /** 公共支付参数 */
  private WxPayCommonProperties common;
  /** App支付参数 */
  private WxAppPayProperties app;
  /** H5支付参数 */
  private WxH5PayProperties h5;
  /** JSAPI支付参数 */
  private WxJsApiPayProperties jsapi;
  /** 扫码支付参数 */
  private WxNativePayProperties nativePay;
  /** 付款码支付参数 */
  private WxPaymentCodePayProperties paymentCode;
  /** 小程序支付参数 */
  private WxMiniProgramPayProperties miniProgram;
}
