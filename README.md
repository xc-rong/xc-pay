# xc-pay
### 介绍
  
    封装微信/支付宝 支付、提现等功能项目进行开源欢迎大家一起改造。

### 项目历程

    1.0. 项目建立初，提供简单boot项目，实现支付宝微信(v2)版本代码
    1.1. 升级为插件项目，支付宝已迭代升级完成。微信在维护中，v2版本代码还可继续参考。 微信v3版本升级中，后续升级V2版本代码。

### 软件架构

    1.0 springboot简单项目；支持更新
    1.1 原有基础更新为插件形式项目框架

### 使用说明

  > 使用

    目前是  打包项目为可使用jar包，项目引入，然后配置所需参数。支付宝参考(AliPayProperties.java).微信参考(WeChatPayProperties.java),目前微信在维护升级中，暂不支持使用。
    -------------------------------------------------------
    故目前之说支付宝使用：
      配置好参数之后，在所需要使用的类中注入AliPayTemplate类，使用该类调用所需要的方法，传递所需要的参数，实现使用。


  > 项目目录结构：

    com:
      xc：
        pay:
          ali:
            constant: 支付宝所需要的一些常量信息
            enums: 支付宝支付/提现时 所需部分参数的抽取成枚举方便使用
            modal: 用于支付宝相关信息的一些参数验证实体封装
            util: 封装支付宝的参数校验工具类和回调验签工具
            AliPayReqeustUtil.java： 用于公共的请求发送
            AliPayTemplate.java: 模板类，用于后续调用方法使用
            AliPayTest.java：测试类
          common: 公用的一些工具
     ---------------------------------------------------------
          # 因维护中，暂不更新以下目录结构，请谅解。v2版本相关支付提现代码可供参考。     
          weixin: 
              app: 微信APP支付
              applet: 微信小程序支付
              h5: 微信H5支付
              jsAPI: 微信公众号支付
              modal: 微信支付的一些实体类
              util: 微信支付相关的一些工具类
              withdraw: 微信提现
              wx_native: 微信扫码支付
          

#### 联系QQ/wx

    QQ: 2598836529
    WX: rong_java

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
